//app.js
import { request } from './utils/api' 
App({
  onLaunch: function () {
    // 展示本地存储能力
    // var logs = wx.getStorageSync('logs') || []
    // logs.unshift(Date.now())
    // wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        request({
          url: '/passengerTraffic-applet/applet/user/getappid',
          method: 'POST',
          data: {
            code: res.code
          }
        }).then(res1 => {
          console.log(res1);
          let codeData = {}
          codeData.openid = res1.data.openid
          codeData.session_key =  res1.data.session_key
          wx.setStorage({
            data: codeData,
            key: 'codeData',
          })
          
        })
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  globalData: {
    userInfo: null,
    statusBarHeight: wx.getSystemInfoSync()['statusBarHeight'],
    // 选择城市 县区时保存全局变量
    selectAddr: null,
    // 路线规划 选择起点时
    startData: null,
    endData: null,
    // 路线规划 选择起点或者终点判断符
    tag: '',
    flag: null,
    // 用来判断跳转城市选择页面是  首页跳的 还是地图搜索页面跳的
    goCityWay: '',
    // 用来保存路线详情全部所需参数
    pathDetailParams: {},
    // 用户的定位 县区
    locationAddr: {},
    // 图片基础路径
    // imgBaseUrl: 'http://192.168.2.175:7071/',
    imgBaseUrl: 'https://gongjiao.hnxtgps.cn/',
    
  }
})