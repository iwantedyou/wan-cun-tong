export const request = (params) => {
  return new Promise((resolve, reject) => {
    // let baseUrl = "http://192.168.2.170:7071"
    // let baseUrl = "http://192.168.2.180:7071"
    let baseUrl = "https://gongjiao.hnxtgps.cn"
    wx.request({
      url: baseUrl + params.url, 
      data: params.data,
      method: params.method,
      timeout: 1000 * 90,
      header: {
        'content-type': "application/x-www-form-urlencoded"
      },
      success(res) {
        resolve(res.data)
      },
      fail(err) {
        reject(err);
        wx.showToast({
          title: '暂无数据!',
          icon: 'none'
        })
      }
    })
  })
}
function dateDiff(firstDate, secondDate) {
  var firstDate = new Date(firstDate);
  var secondDate = new Date(secondDate);
  var diff = Math.abs(firstDate.getTime() - secondDate.getTime())
  var result = parseInt(diff / (1000 * 60 * 60 * 24));
  return result
}

