const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

const formatDate = (now) => { 
  var y = now.getFullYear();
  var m = now.getMonth() - 0 + 1;
  var d = now.getDate();
  var h = now.getHours(); 
  var min = now.getMinutes(); 
  var s = now.getSeconds();

  return y + "-" + formatNumber(m) + "-" + formatNumber(d) + " " + formatNumber(h) + ":" + formatNumber(min); 
} 
module.exports = {
  formatTime: formatTime,
  formatDate
}
