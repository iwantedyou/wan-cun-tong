// pages/home/cities/cities.js
const app = getApp()
import { request } from '../../../utils/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight+45, 
    // 当前位置
    city: '',
    // 城市列表
    cityList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let city = options.city
    this.setData({
      city: city
    })
    request({
      url: '/passengerTraffic-applet/applet/chinaApi/selectCity',
      method:'POST',
      data: {
        flag: 2
      }
    }).then(res => {
      console.log(res);
      this.setData({
        cityList: res.data
      })
    })
  },
  goBack: function() {
    wx.navigateBack({
      delta: 1
    })
  },
  goCounty: function(e) {
    // console.log(e);
    let city = e.currentTarget.dataset.city
    let prov = e.currentTarget.dataset.prov
    // 此时应该跳转到区  县  的选择   传递选择的城市数据
    wx.navigateTo({
      url: '/pages/home/county/county?city=' + city + '&prov=' + prov,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})