
const app = getApp()
const util = require('../../../utils/WSCoordinate')
import { request } from '../../../utils/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight+45, 
     // 用户定位
     latitude:'',
     longitude: '',
    // 待路线详情
    showPath: 'visible',
    // 不带路线详情的
    showPath1: 'hidden',
    // 线路名称
    lineName: '',
    // 线路,
    lineId: '',
    // 路线上行下行
    sortType: 1,
    // 线路详情
    pathDesc: {},
    // 线路站点集合
    stations: [],
    // 地图标记集合
    markers: [],
    polyline: [],
    // 缩小视野索要包含的所有坐标
    points: [],
    // 从哪个标记跳转过来的
    mkId: '',
    // 用户图标显示的坐标
    // widthNum: '',
    // 车辆位置信息
    carAddrData: {},
    // 车辆位置显示坐标
    carWidthNum: '',
    // 显示第几个站点的用户图标
    showIndex: ''
    
    
  },
  // 视野发生变化时触发，
  regionchange(e) {
    // console.log(e.type)
  },
  // 点击标记点时触发
  markertap(e) {
    console.log(e.detail.markerId)
    
  },
  // 点击控件时触发
  controltap(e) {
    console.log(e.detail.controlId)
  },
  hiddenPath() {
    this.setData({
      showPath: 'hidden',
      showPath1: 'visible'
    })
  },
  showPath() {
    this.setData({
      showPath: 'visible',
      showPath1: 'hidden'
    })
  },
  // 换方向
  reversal() {
    clearTimeout(this.data.timer)
    this.setData({
      sortType: this.data.sortType == 1 ? 2 : 1,
    })
    this.getPathData(this.data.lineId, this.data.mkId)
  },
  goBack() {
    wx.navigateBack({
      delta: 1
    })
  },
  // 封装 路线各数据请求
  getPathData(lineId, mkId) {
    let that = this
    request({
      url: '/passengerTraffic-applet/applet/indexApi/lineStation',
      method: 'POST',
      data: {
        lineId,
        stationId: mkId,
        sortType: this.data.sortType
      }
    }).then(res => {
      console.log(res);
      if(res.code == 3) {
        wx.showToast({
          title: res.msg,
          icon: 'none',
          duration: 3500
        })
        this.setData({
          sortType: this.data.sortType == 1 ? 2 : 1
        })
        return
      }
      // 站点坐标集合
      let mks = []
      // 地图视野包含所有图标点的坐标
      let points1 = []
      // 用户点击站点参考的站点索引
      let stationIndex = ''
      // 汽车位置所参考的站点索引
      let carStationIndex = ''
      // 站点坐标集合 字符串
      // let str = ''
      let zbList = []
      res.data.stationList.filter((item, index) => {
        // 统一转换站点坐标系
        //  str = str + item.stationLatitude + ',' + item.stationLongitude + ';'
        let zbObj = util.transformFromWGSToGCJ(item.stationLatitude - 0, item.stationLongitude - 0)
        let obj = {
          callout: {
            content: item.stationName,
            padding: 5,
            borderRadius: 3,
            display: 'BYCLICK'
          },
          iconPath: "/assets/images/bus.png",
          id: item.lsStationId,
          latitude: '',
          longitude: '',
          width: 18,
          height: 26
        }
        let obj1 = {
          latitude: item.stationLatitude,
          longitude: item.stationLongitude,
        }
        if (item.lsStationId == this.data.mkId) {
          stationIndex = index
          that.setData({
            showIndex: index
          })
        } 
        if (item.stationName == res.data.vehicleMessage.flagName) {
          carStationIndex = index
        } 
        mks.push(obj)
        // 需要包含所有图标的站点
        points1.push(obj1)
        zbList.push(zbObj)
      })
      // console.log(str);
      // str = str.slice(0, -1)
      // 坐标系转化  所有站点的坐标
      // wx.request({
      //   url: 'https://apis.map.qq.com/ws/coord/v1/translate?locations='+ str +'&type=1&key=MSPBZ-V5ARU-7VXVP-4MSXQ-KD2MZ-WWBMP',
      //   method: 'get',
      //   success(res2) {
      //     console.log(res2);
          for (let i = 0; i < mks.length; i++) {
              mks[i].latitude = zbList[i].latitude
              mks[i].longitude = zbList[i].longitude
          }
          
          if(res.data.vehicleMessage.message == 1) {
            // 坐标系转换   gps 转换成gcj02
            // let newLat = ''
            // let newLng = ''
            let carZbObj = {}
            // wx.request({
            //   url: 'https://apis.map.qq.com/ws/coord/v1/translate?locations='+res.data.vehicleMessage.vehicleLat+','+res.data.vehicleMessage.vehicleLon+'&type=1&key=MSPBZ-V5ARU-7VXVP-4MSXQ-KD2MZ-WWBMP',
            //   success(res1) {
            //     console.log(res1);
            //     newLat = res1.data.locations[0].lat
            //     newLng = res1.data.locations[0].lng
                carZbObj = util.transformFromWGSToGCJ(res.data.vehicleMessage.vehicleLat - 0, res.data.vehicleMessage.vehicleLon - 0)
                mks.push({
                  iconPath: "/assets/images/busAddr.png",
                  id: 9999999999,
                  latitude: carZbObj.latitude,
                  longitude: carZbObj.longitude,
                  width: 30,
                  height: 18
                })
                that.setData({
                  markers: mks,
                })
            //   }
            // })
          }
          that.setData({
            markers: mks
          })
      //   }
      // })
      // 地图站点连线
      // let polyline1 = [{
      //   points: points1,
      //   color:"#4881fd",
      //   width: 10,
      //   borderWidth: 2,
      //   borderColor: '#333',
      // }]
      let carWidthNum1 =  ''
      if(res.data.vehicleMessage.pointType == 1) {
        carWidthNum1 = carStationIndex * 120 - 52
      } else if (res.data.vehicleMessage.pointType == 2) {
        carWidthNum1 = carStationIndex * 120
      } else if (res.data.vehicleMessage.pointType == 3) {
        carWidthNum1 = carStationIndex * 120 + 52
      }
      this.setData({
        start: res.data.lineName.split('-')[0],
        end: res.data.lineName.split('-')[1],
        pathDesc: res.data,
        stations: res.data.stationList,
        // polyline: polyline1,
        points: points1,
        // widthNum: stationIndex * 120,
        carAddrData: res.data.vehicleMessage,
        carWidthNum: carWidthNum1,
        lineName: res.data.lineName
      })
    })
    that.reLoad(lineId, mkId)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let lineId = options.lineId
    // 标记id 是为了计算显示路线轨迹图上用户所在位置
    let mkId = options.mkId
    let type = options.type
    let that = this
    this.setData({
      lineId: lineId,
      mkId: mkId,
      sortType: type
    })
    // 获取定位
    wx.getLocation({
      success(res) {
        console.log(res);
        that.setData({
          latitude: res.latitude,
          longitude: res.longitude
        })
        that.getPathData(lineId, mkId)
      }
    })
  },
  reLoad(lineId, mkId) {
    let that = this
    var timer = setTimeout(function() {
      that.getPathData(lineId, mkId)
    }, 30000)
    this.setData({
      timer
    })
  },
  onHide: function() {
    console.log('hide');
  },
  onUnload: function() {
    // 页面销毁钩子函数时 销毁定时器
    console.log('onun');
    clearTimeout(this.data.timer)
  }
})