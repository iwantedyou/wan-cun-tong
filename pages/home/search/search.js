import { request } from "../../../utils/api";

// pages/home/search/search.js
const app = getApp()
const chooseLocation = requirePlugin('chooseLocation');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight+45, 
    // 选择的起点位置
    startAddr: '请选择起点',
    // 起点站点id
    startId: '',
    // 起点坐标
    startLat: '',
    startLng: '',
    // 选择的重点位置
    endAddr: '请选择终点',
    // 终点站点id
    endId: '',
    endLat: '',
    endLng: '',
    // 判断选择的是起点还是终点  1 起点  2 终点
    tag: 1,
    // 线路集合
    pathList: [],
    showPaths: 'hidden'
  },
  goBack() {
    wx.navigateBack({
      delta: 1
    })
  },
  start() {
    // this.setData({
    //   tag: 1
    // })
    // const key = 'DUFBZ-OYF6D-RYR4L-HYKJD-JUR7Z-SLFQ5'; //使用在腾讯位置服务申请的key
    // const referer = 'bus'; //调用插件的app的名称
    // const location = JSON.stringify({
    //   latitude: this.data.latitude,
    //   longitude: this.data.longitude
    // });
    // // const category = '生活服务,娱乐休闲';
    // wx.navigateTo({
    //   url: `plugin://chooseLocation/index?key=${key}&referer=${referer}&location=${location}`
    // });
    app.globalData.tag = 1
    wx.redirectTo({
      url: '/pages/searchMap/searchMap?tag=1',
    })
  },
  end() {
    // this.setData({
    //   tag: 2
    // })
    // const key = 'MSPBZ-V5ARU-7VXVP-4MSXQ-KD2MZ-WWBMP'; //使用在腾讯位置服务申请的key
    // const referer = 'bus'; //调用插件的app的名称
    // const location = JSON.stringify({
    //   latitude: this.data.latitude,
    //   longitude: this.data.longitude
    // });
    // // const category = '生活服务,娱乐休闲';
    // wx.navigateTo({
    //   url: `plugin://chooseLocation/index?key=${key}&referer=${referer}&location=${location}`
    // });
    app.globalData.tag = 2
    wx.redirectTo({
      url: '/pages/searchMap/searchMap?tag=2',
    })
  },
  change() {
    if(this.data.startAddr == '请选择起点' || this.data.endAddr == '请选择终点') {
      wx.showToast({
        title: '请选择起点和终点',
        icon: 'none'
      })
      return
    }
    this.setData({
      // startAddr: this.data.endAddr,
      // startId: this.data.endId,
      // endAddr: this.data.startAddr,
      // endId: this.data.startId
    })
  },
  search() {
    if(this.data.startAddr == '请选择起点' || this.data.endAddr == '请选择终点') {
      wx.showToast({
        title: '请选择起点和终点',
        icon: 'none'
      })
      return
    }
    request({
      url: '/passengerTraffic-applet/applet/indexApi/searchLine',
      method: 'POST',
      data: {
        fromLon: this.data.startAddr,
        fromLat: this.data.startLat,
        fromName: this.data.startLng,
        toLon: this.data.endLng,
        toLat: this.data.endLat,
        toName: this.data.endAddr,
        stationCountry: this.data.country
      }
    }).then(res => {
      console.log(res);
      
    })
    // wx.navigateTo({
    //   url: '/pages/pathPlan/pathPlan?fromLon='+this.data.startLng+'&fromLat='+this.data.startLat+'&toLon='+this.data.endLng+'&toLat='+this.data.endLat+'&stationCountry ='+this.data.country+'&fromName='+this.data.startAddr+'&toName='+this.data.endAddr
    // })


    // request({
    //   url: '/passengerTraffic-applet/applet/indexApi/searchLine',
    //   method: 'POST',
    //   data: {
    //     startId: this.data.startId,
    //     endId: this.data.endId
    //   }
    // }).then(res => {
    //   console.log(res);
    //   // 吧路线Id传过去
    //   this.setData({
    //     pathList: res.data,
    //     showPaths: 'visible'
    //   })
    //   // wx.navigateTo({
    //   //   url: '/pages/home/pathDetail/pathDetail',
    //   // })
    // })
    
  },
  goPathDetail(e) {
    let lineId = e.currentTarget.dataset.lineid
    let type = e.currentTarget.dataset.type
    wx.navigateTo({
      url: '/pages/home/pathDetail/pathDetail?lineId=' + lineId + '&mkId=' + this.data.startId + '&type=' + type,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    let that = this
    if(options.country) {
      this.setData({
        country: options.country
      })
    }
    if(app.globalData.startData) {
      this.setData({
        startAddr: app.globalData.startData.title,
        // startId:app.globalData.startData.stationId
        startLat:app.globalData.startData.latitude,
        startLng:app.globalData.startData.longitude
      })
    } 
    if(app.globalData.endData) {
      this.setData({
        endAddr: app.globalData.endData.title,
        // endId:app.globalData.endData.stationId
        endLat:app.globalData.endData.latitude,
        endLng:app.globalData.endData.longitude
      })
    } 
    
    // wx.getLocation({
    //   success(res) {
    //     that.setData({
    //       latitude: res.latitude,
    //       longitude: res.longitude
    //     })
    //   }
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // const location = chooseLocation.getLocation(); // 如果点击确认选点按钮，则返回选点结果对象，否则返回null
    // console.log(location);
    // if(location && this.data.tag == 1) {
    //   this.setData({
    //     startAddr: location.name
    //   })
    // }
    // if(location && this.data.tag == 2) {
    //   this.setData({
    //     endAddr: location.name
    //   })
    // }

  }

})