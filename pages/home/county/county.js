// pages/home/county/county.js
import { request } from '../../../utils/api.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight+45, 
    // 县区集合
    counties: [],
    city: ''
  },
  goIndex: function(e) {
    console.log(e);
    
    let city = e.currentTarget.dataset.city
    let country = e.currentTarget.dataset.country
    app.globalData.selectAddr = {
      city,
      country
    }
    if( app.globalData.goCityWay == 1 ) {
      // 如果是从首页来选市县的
      // 此时跳转到首页 并吧选择的城市  区县数据 传递过去
      wx.switchTab({
        url: '/pages/index/index',
      })
    } else {
      wx.redirectTo({
        url: '/pages/searchMap/searchMap',
      })
    }
    
  },
  goBack() {
    wx.navigateBack({
      delta: 1
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let city = options.city
    let prov = options.prov
    this.setData({
      city
    })
    request({
      url: '/passengerTraffic-applet/applet/chinaApi/selectCountry',
      method: 'POST',
      data: {
        flag: 3,
        province: prov,
        city: city
      }
    }).then(res => {
      console.log(res);
      this.setData({
        counties: res.data
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})