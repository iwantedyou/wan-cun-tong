// pages/mine/newsDetail/newsDetail.js
const app = getApp()
import { request } from '../../../utils/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight + 46,
     // 图片基础路径
     imgBaseUrl: app.globalData.imgBaseUrl,
     detail: {}
  },
  goBack() {
    wx.navigateBack({
      delta: 1,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let newsId =  options.newsId
    request({
      url: '/passengerTraffic-applet/applet/newsLostFound/selectNewsDesc',
      method: 'post',
      data: {
        id: newsId
      }
    }).then(res => {
      console.log(res);
      if(res.code == 1) {
        res.data.content = res.data.content.replace(/\<img/gi, '<img style="max-width:100%;height:auto;margin-top:5px;" ')
        this.setData({
          detail: res.data.content
        })
      }
      
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})