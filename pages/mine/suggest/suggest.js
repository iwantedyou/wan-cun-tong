// pages/mine/suggest/suggest.js
const app = getApp()
import { request } from '../../../utils/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight+45, 
    active: 0,
    // 文本框内容
    content: '',
    userId: '',
    obj: {
      maxHeight: 200,
      minHeight: 100
    },
    dataList: [],
    // 运营公司列表
    companyList: [],
    // 选择的运营公司Id
    companyId: '',
    // 运营公司选择器   显示的索引
    index: '0',
    yunYingCity: ''
  },
  goBack() {
    wx.navigateBack({
      delta: 1
    })
  },
  // tab栏 点击事件
  onChange(event) {
    let that = this
    console.log(event);
    
    if(event.detail.title == '投诉历史') {
      request({
        url: '/passengerTraffic-applet/applet/suggestionApi/suggestionList',
        method: 'POST',
        data: {
          userId: this.data.userId,
          yunyingCity: this.data.yunYingCity
        }
      }).then(res => {
        console.log(res);
        that.setData({
          dataList: res.data
        })
      })
    }
  },
  onText(e) {
    console.log(e);
    this.setData({
      content: e.detail
    })
  },
  // 提交
  submit() {
    request({
      url: '/passengerTraffic-applet/applet/suggestionApi/save',
      method: 'POST',
      data: {
        userId: this.data.userId,
        question: this.data.content,
        yunyingCity: this.data.yunYingCity
      }
    }).then(res => {
      console.log(res);
      if(res.code == 1) {
        wx.showToast({
          title: '提交成功',
        })
        this.setData({
          question: ''
        })
      } else {
        wx.showToast({
          title: res.msg,
          icon: 'none'
        })
      }
    })
  },
  // 运营公司列表选择
  // bindPickerChange(event) {
  //   console.log(event);
  //   this.setData({
  //     index: event.detail.value,
  //     companyId: this.data.companyList[event.detail.value].id
  //   })
  // },
  // 获取运营公司
  // getCompanyList() {
  //   request({
  //     url: '/passengerTraffic-applet/applet/custom/selectYunyingList',
  //     method: 'post',
  //     data: {}
  //   }).then(res => {
  //     console.log(res);
  //     if (res.code == 1) {
  //       let arr = []
  //       res.data.filter(item => {
  //         let obj = {}
  //         obj.id = item.id
  //         obj.name = item.username
  //         arr.push(obj)
  //       })
  //       this.setData({
  //         companyList: arr,
  //         companyId: arr[0].id
  //       })
  //     }
  //   })
  // },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    // wx.getStorage({
    //   key: 'codeData',
    //   success(res) {
    //     console.log(res);
    //     request({
    //       url: '/passengerTraffic-applet/applet/user/selUserByOpenId',
    //       method: 'POST',
    //       data: {
    //         openId: res.data.openid
    //       }
    //     }).then(res1 => {
    //       console.log(res1);
    //       // 吧userId保存在storage里
    //       wx.setStorage({
    //         data: res1.data.id,
    //         key: 'userId',
    //       })
    //       that.setData({
    //         userId: res1.data.id
    //       })
    //     })
    //   }
    // })
    wx.getStorage({
      key: 'userId',
      success(res) {
        that.setData({
          userId: res.data
        })
      }
    })
    // this.getCompanyList()
    var selectAddr = getApp().globalData.selectAddr
    if(!selectAddr) {
      return
    }
    this.setData({
      yunYingCity: selectAddr.country
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})