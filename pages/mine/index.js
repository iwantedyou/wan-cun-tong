// pages/mine/index.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight+45
   
  },
  // 跳转约车订单
  goYueCheOrder() {
    wx.navigateTo({
      url: '/pages/yueche/order/order',
    })
  },
  // 跳转定制订单
  goDingZhiOrder() {
    wx.navigateTo({
      url: '/pages/dingzhi/order/order',
    })
  },
  goHelp() {
    wx.navigateTo({
      url: '/pages/mine/help/help',
    })
  },
  goAbout() {
    wx.navigateTo({
      url: '/pages/mine/about/about',
    })
  },
  // 跳转到失物招领
  goShiWu() {
    wx.navigateTo({
      url: '/pages/mine/shiWu/shiWu',
    })
  },
  // 跳转到新闻资讯
  goNews() {
    wx.navigateTo({
      url: '/pages/mine/news/news',
    })
  },
  goPay() {
    wx.scanCode({
      // 是否只能从相机 扫码
      onlyFromCamera: true,
      success (res) {
        console.log(res)
        console.log(res.result);
        // wx.navigateTo({
        //   url: res.result,
        // })
      }
    })
   
  },
  goSuggest() {
    wx.navigateTo({
      url: '/pages/mine/suggest/suggest',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // let that = this
    // wx.getStorage({
    //   key: 'userInfo',
    //   success(res) {
    //     that.setData({
    //       userInfo: res.data
    //     })
    //   }
    // })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this
    wx.getStorage({
      key: 'phone',
      fail() {
        wx.showModal({
          title: '提示',
          content: '请先登录，再进行操作！',
          showCancel: true,
          cancelColor: '',
          cancelText: '暂不登录',
          confirmText: '立即登录',
          success (res) {
            if (res.confirm) {
              // console.log('用户点击立即登录')
              wx.navigateTo({
                url: '/pages/login/login',
              })
            } else if (res.cancel) {
              // console.log('用户点击暂不登录')
              wx.switchTab({
                url: '/pages/index/index',
              })
            }
          },
          fail (e) {
            console.log(e);
          }
        })
      }
    })
    wx.getStorage({
      key: 'userInfo',
      success(res) {
        that.setData({
          userInfo: res.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})