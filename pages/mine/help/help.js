// pages/mine/help/help.js
const app = getApp()
import { request } from '../../../utils/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight+45, 
    titleList: []
  },
  goBack() {
    wx.navigateBack({
      delta: 1
    })
  },
  // 跳到路线搜索    静态页面时写了三个页面  动态渲染时 一个页面模板就够了 所以就一个事件
  goSearchPath(e) {
    console.log(e);
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/mine/help/searchPath/searchPath?id=' + id,
    })
  },
  // goCheckCar() {
  //   wx.navigateTo({
  //     url: '/pages/mine/help/checkCar/checkCar',
  //   })
  // },
  // gobaocheServe() {
  //   wx.navigateTo({
  //     url: '/pages/mine/help/baocheServe/baocheServe',
  //   })
  // },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var selectAddr = getApp().globalData.selectAddr
    if(!selectAddr) {
      return
    }
    request({
      url: '/passengerTraffic-applet/applet/problem/selProblem',
      method: 'POST',
      data: {
        page: 1,
        size: 99,
        type: 1,
        yunyingCity: selectAddr.country
      }
    }).then(res => {
      console.log(res);
      // 帮助页面 标题数据
      this.setData({
        titleList: res.data
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})