// pages/mine/news/news.js
const app = getApp()
import { request } from '../../../utils/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight + 46,
    // 新闻列表
    newsList: [],
    // 图片基础路径
    imgBaseUrl: app.globalData.imgBaseUrl
  },
  // 跳转到信息详情
  goNewsDetail(e) {
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/mine/newsDetail/newsDetail?newsId=' + id,
    })
  },
  goBack() {  
    wx.navigateBack({
      delta: 1,
    })
  },
  // 获取新闻列表
  getNewsList(city) {
    request({
      url: '/passengerTraffic-applet/applet/newsLostFound/selectNewsPage',
      method: 'post',
      data: {
        yunyingCity: city,
        page: '1',
        size: '2'
      }
    }).then(res => {
      console.log(res);
      if(res.code == 1) {
        this.setData({
          newsList: res.data
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var selectAddr = getApp().globalData.selectAddr
    if(selectAddr) {
      this.getNewsList(selectAddr.country)
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})