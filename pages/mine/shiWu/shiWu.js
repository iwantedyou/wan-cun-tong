// pages/mine/shiWu/shiWu.js
const app = getApp()
import { request } from '../../../utils/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight + 46,
    // 失物招领列表
    dataList: [],
    // 图片基础路径
    imgBaseUrl: app.globalData.imgBaseUrl
  },
  goBack() {
    wx.navigateBack({
      delta: 1,
    })
  },
  // 图片预览
  onPreview(e) {
    wx.previewImage({
      current: e.currentTarget.dataset.url, // 当前显示图片的http链接
      urls: [e.currentTarget.dataset.url] // 需要预览的图片http链接列表
    })
  },
  // 获取失物招领列表
  getShiWuList(city) {
    request({
      url: '/passengerTraffic-applet/applet/newsLostFound/selectLostFoundPage',
      method: 'post',
      data: {
        yunyingCity: city,
        page: '1',
        size: '99'
      }
    }).then(res => {
      console.log(res);
      if(res.code == 1) {
        res.data.filter(item1 => {
          item1.images = item1.images.split('|')
        })
        this.setData({
          dataList: res.data
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var selectAddr = getApp().globalData.selectAddr
    if(selectAddr) {
      this.getShiWuList(selectAddr.country)
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})