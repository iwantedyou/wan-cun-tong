// pages/searchMap/searchMap.js
import {
  request
} from '../../utils/api'
const app = getApp()
// 小程序地图jdk  自定义搜索地点用
var QQMapWX = require('../../assets/js/qqmap-wx-jssdk.min.js');
const util = require('../../utils/WSCoordinate')
// 实例化API核心类
var qqmapsdk = new QQMapWX({
  key: 'MSPBZ-V5ARU-7VXVP-4MSXQ-KD2MZ-WWBMP' // 必填
})
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight + 45,
    // 地图中心点
    latitude: '',
    longitude: '',
    points: [],
    //是否显示 当前位置
    nowAddr: true,
    markers: [],
    // 输入的搜多地点
    addr: '',
    // 站点集合
    stations: [],
    // 用来判断是起点还是终点条进来的
    tag: '',
    // 移动后的中心点坐标
    cnterLng: '',
    cneterLat: '',
    // 移动地图后 中心点附近的门店信息
    nearShop: [],
    // 定位城市  
    city: '城市',
    country: '区域'
  },
  goBack() {
    wx.switchTab({
      url: '/pages/pathPlan/search/search',
    })
  },
  // 输入框输入事件
  inpAddr(e) {
    // console.log(e);

    this.setData({
      addr: e.detail.value
    })
  },
  // 事件触发，调用接口
  nearby_search: function () {
    if (!this.data.addr) {
      wx.showToast({
        title: '请输入地点',
        icon: 'none'
      })
      return
    }
    // 把显示当前位置关掉  否则中心点跳不过去
    this.setData({
      nowAddr: false
    })
    // // 实例化API核心类
    // var qqmapsdk = new QQMapWX({
    //   key: 'MSPBZ-V5ARU-7VXVP-4MSXQ-KD2MZ-WWBMP' // 必填
    // });
    var _this = this;
    // 调用接口
    qqmapsdk.search({
      region: this.data.country,
      keyword: this.data.addr, //搜索关键词
      page_size: 3, // 数据数量
      auto_extend: '1', // 扩大范围
      // location: '39.980014,116.313972',  //设置周边搜索中心点
      success: function (res) { //搜索成功后的回调
        console.log(res);
        if (res.data.length != 0) {
          var mks = []
          // var arr = []
          for (var i = 0; i < res.data.length; i++) {
            // mks.push({ // 获取返回结果，放到mks数组中
            //   title: res.data[i].title,
            //   id: res.data[i].id,
            //   latitude: res.data[i].location.lat,
            //   longitude: res.data[i].location.lng,
            //   iconPath: "/assets/images/addr.png", //图标路径
            //   width: 20,
            //   height: 40
            // })
            // arr.push({
            //   latitude: res.data[i].location.lat,
            //   longitude: res.data[i].location.lng,
            // })
          }
          _this.setData({ //设置markers属性，将搜索结果显示在地图中
            // markers: mks,
            // points: arr,
            latitude: res.data[0].location.lat,
            longitude: res.data[0].location.lng,

          })
          var mapCtx = wx.createMapContext('map'); //wxml中map标签的id值
          mapCtx.moveToLocation({
            longitude: res.data[0].location.lng,
            latitude: res.data[0].location.lat,
          });
          // 查询搜索的地点附近是否有站点
          request({
            url: '/passengerTraffic-applet/applet/indexApi/nearStation',
            method: 'POST',
            data: {
              longitude: res.data[0].location.lng,
              latitude: res.data[0].location.lat
            }
          }).then(res1 => {
            console.log(res1);
            // 站点显示
            let mks1 = []
            let arr1 = []
            for (var i = 0; i < res1.data.length; i++) {
              let obj = util.transformFromWGSToGCJ(res1.data[i].stationLatitude - 0, res1.data[i].stationLongitude - 0)
              mks1.push({ // 获取返回结果，放到mks数组中
                callout: {
                  content: res1.data[i].stationName,
                  padding: 5,
                  borderRadius: 3,
                  display: 'BYCLICK',
                  borderColor: '#1f9ae4',
                  borderWidth: 1
                },

                id: res1.data[i].stationId,
                latitude: obj.latitude,
                longitude: obj.longitude,
                iconPath: "/assets/images/bus4.png", //图标路径
                width: 20,
                height: 20
              })
              arr1.push({
                latitude: obj.latitude,
                longitude: obj.longitude,
              })
            }
            _this.setData({
              stations: res1.data,
              markers: mks1,
              points: arr1
            })
          })
        }
      },
      fail: function (res) {
        console.log(res);
      },
      complete: function (res) {
        // console.log(res);
      }
    });
  },

  // 点击地图时触发
  selectAddr(e) {
    console.log(e.detail);
    // let lat = e.detail.latitude.toFixed(6)
    // let long = e.detail.longitude.toFixed(6)
    // console.log(lat, long);

    // this.setData({
    //   markers: [
    //     {
    //       iconPath: "/assets/images/userAddr.png",
    //       id: 0,
    //       latitude: lat,
    //       longitude: long,
    //       width: 25,
    //       height: 30
    //     }
    //   ]
    // })
  },
  // 视野发生变化时触发，  缩小  移动 都会触发
  regionchange(e) {
    console.log(e.type)
    if (e.type == 'end') {

      let MapContext = wx.createMapContext('map')
      MapContext.getCenterLocation({
        success: (res) => {
          console.log(res);
          // 此时返回的已经是gcj 无需转化 
          // let obj = util.transformFromGCJToWGS(res.latitude - 0, res.longitude - 0)    // gps坐标  调试时需要的
          this.setData({
            cneterLat: res.latitude,
            cnterLng: res.longitude,
            // gpsLat: obj.latitude,
            // gpsLng: obj.longitude
          })
          request({
            url: '/passengerTraffic-applet/applet/indexApi/nearSearch',
            method: 'POST',
            data: {
              centerLon: res.longitude,
              centerLat: res.latitude,
              page_size: '30',
              page_index: '1',
              key: 'MSPBZ-V5ARU-7VXVP-4MSXQ-KD2MZ-WWBMP'
            }
          }).then(res11 => {
            console.log(res11);
            this.setData({
              nearShop: res11.data.data
            })
          })
          // 获取地图  坐下  右上 角坐标  矩形搜索
          // MapContext.getRegion({
          //   success: (res1) => {
          //     // console.log(res1);

          //     // 实例化API核心类
          //     var qqmapsdk = new QQMapWX({
          //       key: 'MSPBZ-V5ARU-7VXVP-4MSXQ-KD2MZ-WWBMP' // 必填
          //     });
          //     var _this = this;
          //     // 调用接口
          //     qqmapsdk.search({
          //       keyword: '',  //搜索关键词
          //       location: {
          //         latitude: res.latitude,
          //         longitude: res.longitude
          //       },  //设置周边搜索中心点
          //       // rectangle: `${res1.southwest.latitude},${res1.southwest.longitude},${res1.northeast.latitude},${res1.northeast.longitude}`,
          //       auto_extend: '0',
          //       page_size: '30',
          //       success: function (res2) { //搜索成功后的回调

          //         console.log(res2);
          //         _this.setData({
          //             nearShop: res2.data
          //         })
          //       },
          //       fail: function (res) {
          //         console.log(res);
          //       },
          //       complete: function (res){
          //         // console.log(res);
          //       }
          //   });

          //   }
          // })

        }
      })
    }

  },
  // 点击标记点时触发
  markertap(e) {
    console.log(e)

  },
  // 点击控件时触发
  controltap(e) {
    console.log(e.detail.controlId)
  },
  dingwei() {
    let that = this
    // 回到定位点  方法一
    // wx.getLocation({
    //   success(res) {
    //     console.log(res);
    //     let obj = util.transformFromWGSToGCJ(res.latitude - 0, res.longitude - 0)
    //     that.setData({
    //       latitude: obj.latitude,
    //       longitude: obj.longitude,
    //       nowAddr: true
    //     })
    //   }
    // })

    this.setData({
      nowAddr: true
    }, () => {
      // 回到定位点  方法二
      let mapCtx = wx.createMapContext('map'); //wxml中map标签的id值
      mapCtx.moveToLocation();
      // this.onLoad()
    })
  },
  // 点击站点跳转到搜索界面
  // goSearch(e) {
  //   console.log(e);
  //   if(this.data.tag == 1) {
  //     getApp().globalData.startData = {
  //       stationName: e.currentTarget.dataset.sname,
  //       stationId: e.currentTarget.dataset.sid
  //     }
  //   }
  //   if(this.data.tag == 2) {
  //     getApp().globalData.endData = {
  //       stationName: e.currentTarget.dataset.sname,
  //       stationId: e.currentTarget.dataset.sid
  //     }
  //   }
  //   wx.redirectTo({
  //     url: '/pages/home/search/search?tag='+ this.data.tag,
  //   })
  // },
  // 新版   点击搜索出的poi时
  goSearch1(e) {
    // console.log(e);
    // console.log(app.globalData);

    if (this.data.tag == 1 || app.globalData.tag == 1) {
      console.log('app');

      getApp().globalData.startData = {
        // stationName: e.currentTarget.dataset.sname,
        // stationId: e.currentTarget.dataset.sid
        latitude: e.currentTarget.dataset.lat,
        longitude: e.currentTarget.dataset.lng,
        title: e.currentTarget.dataset.title
      }
    }
    if (this.data.tag == 2 || app.globalData.tag == 2) {
      getApp().globalData.endData = {
        // stationName: e.currentTarget.dataset.sname,
        // stationId: e.currentTarget.dataset.sid
        latitude: e.currentTarget.dataset.lat,
        longitude: e.currentTarget.dataset.lng,
        title: e.currentTarget.dataset.title
      }
    }
    // wx.switchTab({
    //   url: '/pages/pathPlan/search/search?tag='+ this.data.tag + '&country=' + this.data.country,
    // })
    wx.switchTab({
      url: '/pages/pathPlan/search/search',
    })
  },
  // 选择城市
  selectCity() {
    console.log(11);
    app.globalData.goCityWay = 2
    wx.navigateTo({
      url: '/pages/home/cities/cities?city=' + this.data.city,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('onload', app.globalData);
    let that = this
    let tag = options.tag
    var selectAddr = getApp().globalData.selectAddr
    var locationAddr = getApp().globalData.locationAddr
    // console.log(selectAddr);
    if (selectAddr && selectAddr.country != locationAddr.country) {
      this.setData({
        tag: tag,
        city: selectAddr.city,
        country: selectAddr.country
      })
      qqmapsdk.geocoder({
        address: this.data.city + this.data.country,
        success: (res) => {
          console.log(res);
          this.setData({
            latitude: res.result.location.lat,
            longitude: res.result.location.lng
          })
        }
      })
      return
    }
    if (selectAddr && selectAddr.country == locationAddr.country) {
      this.setData({
        tag: tag,
        city: locationAddr.city,
        country: locationAddr.country,
        latitude: locationAddr.latitude,
        longitude: locationAddr.longitude
      })
      return
    }
    wx.getLocation({
      success(res) {
        // console.log(res);
        let obj = util.transformFromWGSToGCJ(res.latitude - 0, res.longitude - 0)
        that.setData({
          tag: tag,
          latitude: obj.latitude,
          longitude: obj.longitude
        })
        // request({
        //   url: '/passengerTraffic-applet/applet/indexApi/nearStation',
        //   method: 'POST',
        //   data: {
        //     longitude: obj.longitude,
        //     latitude: obj.latitude
        //   }
        // }).then(res1 => {
        //   console.log(res1);
        //   // 站点显示
        //   let mks2 = []
        //   let arr2 = []
        //   for (var i = 0; i < res1.data.length; i++) {
        //     let obj1 = util.transformFromWGSToGCJ(res1.data[i].stationLatitude - 0, res1.data[i].stationLongitude - 0)
        //     mks2.push({ // 获取返回结果，放到mks数组中
        //       callout: {
        //         content: res1.data[i].stationName,
        //         padding: 5,
        //         borderRadius: 3,
        //         display: 'BYCLICK'
        //       },
        //       id: res1.data[i].stationId,
        //       latitude: obj1.latitude,
        //       longitude: obj1.longitude,
        //       iconPath: "/assets/images/bus.png", //图标路径
        //       width: 20,
        //       height: 30
        //     })
        //     arr2.push({
        //       latitude: obj1.latitude,
        //       longitude: obj1.longitude,
        //     })
        //   }
        //   that.setData({
        //     stations: res1.data,
        //     tag: tag,
        //     markers: mks2,
        //     points: arr2
        //   })
        // })
      }
    })
  },
  onShow111() {
    var selectAddr = getApp().globalData.selectAddr
    console.log('onshow', selectAddr);
    if (selectAddr) {
      this.setData({
        city: selectAddr.city,
        country: selectAddr.country
      })
      qqmapsdk.geocoder({
        address: this.data.city + this.data.country,
        success: (res) => {
          console.log(res);
          this.setData({
            latitude: res.result.location.lat,
            longitude: res.result.location.lng
          })
        }
      })
      // request({
      //   url: '/passengerTraffic-applet/applet/indexApi/countryStation',
      //   method: 'POST',
      //   data: selectAddr
      // }).then(res => {
      //   console.log(res);

      //   if(res.data != 0) {
      //     let str = ''
      //     let mks = []
      //     let points1 = []
      //     let zbList = []
      //     res.data.filter((item, index )=> {
      //       // str = str + item.stationLatitude + ',' + item.stationLongitude + ';'
      //       mks.push({
      //         callout: {
      //           content: item.stationName,
      //           padding: 5,
      //           borderRadius: 3,
      //           display: 'BYCLICK',
      //           borderColor: '#1f9ae4',
      //           borderWidth: 1
      //         },
      //         iconPath: "/assets/images/bus4.png",
      //         id: item.stationId,
      //         latitude: '',
      //         longitude: '',
      //         width: 20,
      //         height: 20
      //       },)
      //       points1.push({
      //         latitude: item.stationLatitude,
      //         longitude: item.stationLongitude,
      //       })
      //       let obj = util.transformFromWGSToGCJ(item.stationLatitude - 0, item.stationLongitude - 0)
      //       zbList.push(obj)
      //     })
      //     // str = str.slice(0, -1)
      //     // wx.request({
      //     //   url: 'https://apis.map.qq.com/ws/coord/v1/translate?locations='+ str +'&type=1&key=MSPBZ-V5ARU-7VXVP-4MSXQ-KD2MZ-WWBMP',
      //     //   method: 'get',
      //     //   success(res2) {
      //     //     console.log(res2)
      //     // })
      //     for (let i = 0; i < mks.length; i++) {
      //       mks[i].latitude = zbList[i].latitude
      //       mks[i].longitude = zbList[i].longitude
      //     }
      //     // 搜到的第一个站点经纬度当地图中心点   转换一下坐标系
      //     let obj2 = util.transformFromWGSToGCJ(res.data[0].stationLatitude - 0, res.data[0].stationLongitude - 0)
      //     that.setData({
      //       markers: mks,
      //       latitude: obj2.latitude,
      //       longitude: obj2.longitude,
      //       points: points1
      //     })
      //     // var mapCtx = wx.createMapContext('map'); //wxml中map标签的id值
      //     //     mapCtx.moveToLocation({
      //     //       longitude: util.transformFromWGSToGCJ(res.data[0].stationLongitude - 0),
      //     //       latitude: util.transformFromWGSToGCJ(res.data[0].stationLatitude - 0)
      //     //     });
      //     //   }
      //     // })
      //   } else {
      //     console.log('001');
      //     wx.showToast({
      //       title: '该区域暂未开通！',
      //       icon: 'none',
      //       duration: 2500
      //     })
      //   }
      // })
    }
  }
})