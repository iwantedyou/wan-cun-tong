import {
  request
} from "../../utils/api";

// pages/login/login.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showPhone: false,
    showUser: true,
    saveUserInfo: {}
  },
  bindGetUserInfo(e) {
    console.log('用户信息', e);
    this.data.saveUserInfo.username = e.detail.userInfo.nickName
    this.data.saveUserInfo.headimg = e.detail.userInfo.avatarUrl
    wx.setStorage({
      data: this.data.saveUserInfo,
      key: 'userInfo',
    })
    if (e.detail.iv) {
      this.setData({
        showPhone: true,
        showUser: false
      })
    }
  },
  // 授权手机号
  bindGetPhone(e) {
    console.log(e);
    if (e.detail.iv) {
      wx.getStorage({
        key: 'codeData',
        success: (res) => {
          this.data.saveUserInfo.openid = res.data.openid
          // 向后台换取手机号
          request({
            url: '/passengerTraffic-applet/applet/user/deciphering',
            method: 'POST',
            data: {
              encryptedData: e.detail.encryptedData,
              iv: e.detail.iv,
              session_key: res.data.session_key
            }
          }).then(res1 => {
            console.log(res1);
            this.data.saveUserInfo.phone = res1.phoneNumber
            //查询userid  如果新用户  查不到信息  此时 先存 然后再查  拿userId
            request({
              url: '/passengerTraffic-applet/applet/user/selUserByOpenId',
              method: 'POST',
              data: {
                openId: res.data.openid
              }
            }).then(res0 => {
              if (res0.code == 1) {
                wx.setStorage({
                  data: res0.data.id,
                  key: 'userId',
                })
              } else {
                // 发送后台保存用户信息
                request({
                  url: '/passengerTraffic-applet/applet/user/saveUser',
                  method: 'POST',
                  data: this.data.saveUserInfo
                }).then(res2 => {
                  // console.log(res2);
                  // 保存完信息之后 查询userid
                  request({
                    url: '/passengerTraffic-applet/applet/user/selUserByOpenId',
                    method: 'POST',
                    data: {
                      openId: res.data.openid
                    }
                  }).then(res3 => {
                    if (res3.code == 1) {
                      // console.log();
                      wx.setStorage({
                        data: res3.data.id,
                        key: 'userId',
                      })
                    }
                  })
                })
              }
            })

            wx.setStorage({
              data: res1.phoneNumber,
              key: 'phone',
              success() {
                wx.switchTab({
                  url: '/pages/index/index',
                })
              }
            })

          })
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})