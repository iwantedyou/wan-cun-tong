// pages/yueche/order/order.js
const app = getApp()
import {
  request
} from '../../../utils/api'
import Dialog from '../../../vant/dialog/dialog';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight + 45,
    active: 0,
    // 订单类型
    orType: '1',
    // 待处理订单列表
    waitList: [],
    // 进行中订单列表
    onGoing: [],
    // 已取消订单列表
    canceled: [],
    // 已完成订单列表
    finished: [],
    // 页数
    page: 1,
    // 数据数量
    size: 4,
    // 触底是否加载更多
    isLoad: true,
    // 取消订单原因
    cencelReason: '',
    // 取消弹窗开关
    showCencleModel: false,
    openId: ''

  },
  // 点击的取消订单  单号
  cencleOrderNum: '',
  goBack() {
    wx.switchTab({
      url: '/pages/mine/index',
    })
  },
  cancle(e) {
    console.log(e);

    let that = this
    this.cencleOrderNum = e.currentTarget.dataset.id
    this.setData({
      showCencleModel: true
    })
    // wx.showModal({
    //   title: '确定要取消该订单吗',
    //   showCancel: true,
    //   success (res) {
    //     if (res.confirm) {
    //       request({
    //         url: '/passengerTraffic-applet/applet/reserveCar/abolishReserve',
    //         method: 'POST',
    //         data: {
    //           orderId: orNumber
    //         }
    //       }).then(res => {
    //         if(res.code == 1) {
    //           that.onLoad()
    //         }
    //       })
    //     } else if (res.cancel) {
    //       console.log('用户点击取消')
    //     }
    //   }
    // })

  },
  onChange(event) {
    // console.log(event);
    let that = this
    if (event.detail.title == '待处理') {
      this.setData({
        orType: '1',
        page: 1,
        isLoad: true
      })
    } else if (event.detail.title == '进行中') {
      this.setData({
        orType: '2',
        page: 1,
        isLoad: true
      })
    } else if (event.detail.title == '已取消') {
      this.setData({
        orType: '4',
        page: 1,
        isLoad: true
      })
    } else if (event.detail.title == '已完成') {
      this.setData({
        orType: '3',
        page: 1,
        isLoad: true
      })
    }
    this.getOederList()
  },
  // 获取订单列表
  getOederList() {
    request({
      url: '/passengerTraffic-applet/applet/reserveCar/selReserve',
      method: 'POST',
      data: {
        page: 1,
        size: 4,
        type: this.data.orType,
        userId: this.data.userId
      }
    }).then(res => {
      console.log(res);
      if (this.data.orType == 1) {
        this.setData({
          waitList: res.data
        })
      } else if (this.data.orType == 2) {
        this.setData({
          onGoing: res.data
        })
      } else if (this.data.orType == 3) {
        this.setData({
          finished: res.data
        })
      } else if (this.data.orType == 4) {
        this.setData({
          canceled: res.data
        })
      }
    })
  },
  // 支付
  goPay(e) {
    // 先查询是否当地运营支持线上支付
    let companyId = e.currentTarget.dataset.companyid
    request({
      url: '/passengerTraffic-applet/applet/reserveCar/selectIfPay',
      method: 'post',
      data: {
        companyId: companyId
      }
    }).then(res => {
      console.log(res);
      if (res.code != 1) {
        return
      }
      if (res.data.zhifuState == 1) {
        wx.showToast({
          title: '该运营商不支持线上支付',
          icon: 'none'
        })
        return
      }
      if (res.data.zhifuState == 2) {
        // console.log(e);
        let orderId = e.currentTarget.dataset.orderid
        let money = e.currentTarget.dataset.money
        if (money <= 0) {
          return
        }
        request({
          url: '/passengerTraffic-applet/applet/payApi/reservePay',
          method: 'post',
          data: {
            openid: this.data.openId,
            id: orderId
          }
        }).then(res => {
          console.log(res);
          wx.requestPayment({
            nonceStr: res.nonce_str,
            package: res.package,
            paySign: res.paySign,
            timeStamp: res.timeStamp,
            signType: res.signType,
            success: res => {
              console.log(res);
              wx.showToast({
                title: '支付成功',
              })
              // 获取订单列表
              this.getOederList()
            },
            fail: res => {
              console.log(res);
              wx.showToast({
                title: '支付失败',
                icon: 'none'
              })
            }
          })

        })
      }
    })

  },
  // 拨打电话
  onCall(e) {
    console.log(e);
    // currentTarget.dataset.phone
    Dialog.confirm({
        title: '提示',
        message: '确认要拨打电话吗？',
      })
      .then(() => {
        // on confirm
        wx.makePhoneCall({
          phoneNumber: e.currentTarget.dataset.phone,
          fail: res => {
            wx.showToast({
              title: '拨打失败',
              icon: 'none'
            })
          }
        })
      })
      .catch(() => {
        // on cancel
      });
  },
  // 关闭取消原因弹窗
  onClose() {
    this.setData({
      showCencleModel: false
    })
  },
  // 确认取消
  sureCencle() {
    console.log('确认取消');
    if (!this.data.cencelReason.trim()) {
      wx.showToast({
        title: '请输入取消原因',
        icon: 'none'
      })
      return
    }
    request({
      url: '/passengerTraffic-applet/applet/reserveCar/abolishReserve',
      method: 'POST',
      data: {
        orderId: this.cencleOrderNum,
        cancleBecause: this.data.cencelReason
      }
    }).then(res => {
      if (res.code == 1) {
        wx.showToast({
          title: '取消成功',
          icon: 'none'
        })
        this.getOederList()
      } else {
        wx.showToast({
          title: '取消失败',
          icon: 'none'
        })
      }
    })
    this.setData({
      showCencleModel: false,
      cencelReason: ''
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    wx.getStorage({
      key: 'userId',
      success(res) {
        that.setData({
          userId: res.data
        })
        request({
          url: '/passengerTraffic-applet/applet/reserveCar/selReserve',
          method: 'POST',
          data: {
            page: 1,
            size: '4',
            type: that.data.orType,
            userId: res.data
          }
        }).then(res1 => {
          console.log(res1);
          if (res1.code == 1) {
            that.setData({
              waitList: res1.data
            })
          }
        })
      }
    })
    this.setData({
      openId: wx.getStorageSync('codeData').openid
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    // 是否需要发送请求
    if (this.data.isLoad) {
      this.setData({
        page: this.data.page + 1
      })
      request({
        url: '/passengerTraffic-applet/applet/reserveCar/selReserve',
        method: 'POST',
        data: {
          page: this.data.page,
          size: this.data.size,
          type: this.data.orType
        }
      }).then(res => {
        console.log(res);
        if (res.code == 1) {
          if (res.data.length == 0) {
            // 没有更多数据时  关闭触底发送请求
            this.setData({
              isLoad: false
            })
            wx.showToast({
              title: '没有更多数据了',
              icon: 'none'
            })
            return
          }
          if (this.data.orType == 1) {
            this.setData({
              waitList: [...this.data.waitList, ...res.data]
            })
          } else if (this.data.orType == 2) {
            this.setData({
              obGoing: [...this.data.obGoing, ...res.data]
            })
          } else if (this.data.orType == 3) {
            this.setData({
              finished: [...this.data.finished, ...res.data]
            })
          } else if (this.data.orType == 4) {
            this.setData({
              canceled: [...this.data.canceled, ...res.data]
            })
          }

        }
      })
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})