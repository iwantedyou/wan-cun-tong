// pages/route-plan/index.js
const app = getApp()
const chooseLocation = requirePlugin('chooseLocation');
import Dialog from '../../vant/dialog/dialog';
import {
  request
} from '../../utils/api'
import {
  formatDate
} from '../../utils/util'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight + 45,
    // 经纬度  跳转到地图选点需要
    latitude: '',
    longitude: '',
    // 起点
    startAddr: '请选择起点',
    // 起点城市
    startCity: '',
    // 终点
    endAddr: '请选择终点',
    radio: '1',
    // 用车类型   1 约车  2 包车
    type: 1,
    // 判断选择的是起点还是终点  1 起点  2 终点
    tag: null,
    // 时间选择器数据
    minHour: 10,
    maxHour: 20,
    minDate: new Date().getTime(),
    // maxDate: new Date(2019, 10, 1).getTime(),
    currentDate: new Date().getTime(),
    // 是否弹出层
    show: false,
    // 为格化式的时间
    baseTime: '',
    // 选择后的日期格式化
    formatTime: '请选择时间',
    // 预约人数
    peoples: '',
    // 预约方电话
    phone: '',
    // 预约人姓名
    name: '',
    // 备注
    remark: '',
    userId: '',
    // 运营公司列表
    companyList: [],
    // 选择的运营公司Id
    companyId: '',
    // 运营公司选择器   显示的索引
    index: '0',
    // 运营商电话
    yunYingPhone: ''
  },
  showPopup() {
    this.setData({
      show: true
    })
  },
  onClose() {
    this.setData({
      show: false
    });
  },
  // 时间选择器事件
  selectDate(event) {
    // 未格式化的时间格式
    let baseTime = event.detail
    let time = new Date(event.detail)
    let formatTime = time.getFullYear() + "年" + (time.getMonth() - 0 + 1) + "月" + time.getDate() + "日" + time.getHours() + "时" + time.getMinutes() + "分"
    // console.log(formatTime);
    this.setData({
      baseTime: baseTime,
      currentDate: event.detail,
      show: false,
      formatTime: formatTime
    });
    // console.log(event.detail);
  },
  onCancel() {
    this.setData({
      show: false
    })
  },
  // 选择起点
  start() {
    this.setData({
      tag: 1
    })
    const key = 'DUFBZ-OYF6D-RYR4L-HYKJD-JUR7Z-SLFQ5'; //使用在腾讯位置服务申请的key
    const referer = 'bus'; //调用插件的app的名称
    const location = JSON.stringify({
      latitude: this.data.latitude,
      longitude: this.data.longitude
    });
    // const category = '生活服务,娱乐休闲';
    wx.navigateTo({
      url: `plugin://chooseLocation/index?key=${key}&referer=${referer}&location=${location}`
    });
  },
  // 选择终点
  end() {
    this.setData({
      tag: 2
    })
    const key = 'MSPBZ-V5ARU-7VXVP-4MSXQ-KD2MZ-WWBMP'; //使用在腾讯位置服务申请的key
    const referer = 'bus'; //调用插件的app的名称
    const location = JSON.stringify({
      latitude: this.data.latitude,
      longitude: this.data.longitude
    });
    // const category = '生活服务,娱乐休闲';
    wx.navigateTo({
      url: `plugin://chooseLocation/index?key=${key}&referer=${referer}&location=${location}`
    });
  },

  // 弄两个事件  就不用再判断了  简单干脆
  selectType1() {
    this.setData({
      type: 1
    })
  },
  selectType2() {
    this.setData({
      type: 2
    })
  },
  // 输入人数
  peoples(event) {
    this.setData({
      peoples: event.detail.value
    })
  },
  // 输入手机号
  phone(event) {
    this.setData({
      phone: event.detail.value
    })
  },
  // 输入姓名
  name(event) {
    this.setData({
      name: event.detail.value
    })
  },
  // 输入备注
  remark(event) {
    this.setData({
      remark: event.detail.value
    })
  },
  // 运营公司列表选择
  bindPickerChange(event) {
    console.log(event);
    this.setData({
      index: event.detail.value,
      companyId: this.data.companyList[event.detail.value].id
    })
    this.getYunYingPhone(this.data.companyList[event.detail.value].id)
  },
  // 获取运营商电话
  getYunYingPhone(city) {
    request({
      url: '/passengerTraffic-applet/applet/reserveCar/selectYunyingByStartCity',
      method: 'post',
      data: {
        startCity: city
      }
    }).then(res => {
      if (res.code == 1 && res.data.state == 1) {
        this.setData({
          yunYingPhone: res.data.phone
        })
      } else {
        wx.showToast({
          title: '该区域不在运营范围内！',
          icon: 'none',
          duration: 2500
        })
      }
    })
  },
  //提交 跳转到订单详情
  goOrder() {
    if (this.data.startAddr == '请选择起点') {
      wx.showToast({
        title: '请选择起点',
        icon: 'none'
      })
      return
    } else if (this.data.endAddr == '请选择终点') {
      wx.showToast({
        title: '请选择终点',
        icon: 'none'
      })
      return
    } else if (!this.data.baseTime) {
      wx.showToast({
        title: '请选择时间',
        icon: 'none'
      })
      return
    } else if (!this.data.peoples) {
      wx.showToast({
        title: '请输入人数',
        icon: 'none'
      })
      return
    } else if (!this.data.name) {
      wx.showToast({
        title: '请输入姓名',
        icon: 'none'
      })
      return
    } else if (!this.data.phone || !(/^1[345789]\d{9}$/.test(this.data.phone))) {
      wx.showToast({
        title: '请输入正确的手机号',
        icon: 'none'
      })
      return
    } else if (!this.data.yunYingPhone) {
      wx.showToast({
        title: '该区域不在运营范围内！',
        icon: 'none'
      })
      return
    }
    let time2 = new Date(this.data.baseTime)
    time2 = formatDate(time2)
    request({
      url: '/passengerTraffic-applet/applet/reserveCar/saveReserve',
      method: 'POST',
      data: {
        rcStart: this.data.startAddr,
        rcEnd: this.data.endAddr,
        rcUserRuntime: time2,
        rcType: this.data.type,
        rcPeople: this.data.peoples,
        rcPhone: this.data.phone,
        rcUserId: this.data.userId,
        companyId: this.data.companyId,
        rcUsername: this.data.name,
        rcRemark: this.data.remark,
        startCity: this.data.startCity
      }
    }).then(res => {
      console.log(res);
      if (res.code == 1) {
        wx.showToast({
          title: '提交成功',
          success() {
            setTimeout(function () {
              wx.redirectTo({
                url: '/pages/yueche/order/order',
              })
            }, 1000)
          }
        })
      } else {
        wx.showToast({
          title: res.msg,
          icon: 'none'
        })
      }
    })

  },

  // 拨打电话
  onCall(e) {
    console.log(e);
    // currentTarget.dataset.phone
    Dialog.confirm({
        title: '提示',
        message: '确认要拨打电话吗？',
      })
      .then(() => {
        // on confirm
        wx.makePhoneCall({
          phoneNumber: e.currentTarget.dataset.phone,
          fail: res => {
            wx.showToast({
              title: '拨打失败',
              icon: 'none'
            })
          }
        })
      })
      .catch(() => {
        // on cancel
      });
  },

  // 根据起点获取运营商
  getCompany(city) {
    request({
      url: '',
      method: 'post',
      data: {}
    }).then(res => {
      console.log(res.data);
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this
    // 获取定位
    wx.getLocation({
      success(res) {
        console.log(res);
        that.setData({
          latitude: res.latitude,
          longitude: res.longitude
        })
      }
    })
    wx.getStorage({
      key: 'userId',
      success(res1) {
        that.setData({
          userId: res1.data
        })
      }
    })
    // 获取运营公司
    // request({
    //   url: '/passengerTraffic-applet/applet/custom/selectYunyingList',
    //   method: 'post',
    //   data: {}
    // }).then(res => {
    //   console.log(res);
    //   if (res.code == 1) {
    //     let arr = []
    //     res.data.filter(item => {
    //       let obj = {}
    //       obj.id = item.id
    //       obj.name = item.username
    //       arr.push(obj)
    //     })
    //     that.setData({
    //       companyList: arr,
    //       companyId: arr[0].id
    //     })
    //     this.getYunYingPhone(arr[0].id)
    //   }
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const location = chooseLocation.getLocation(); // 如果点击确认选点按钮，则返回选点结果对象，否则返回null
    console.log(location);
    if (location && this.data.tag == 1) {
      this.setData({
        startAddr: location.name,
        startCity: location.district
      })
      this.getYunYingPhone(location.district)
    }
    if (location && this.data.tag == 2) {
      this.setData({
        endAddr: location.name
      })
    }
    // 登录后才能获取到手机号 以此判断用户是否登录
    wx.getStorage({
      key: 'phone',
      success(res) {
        console.log(res);
      },
      fail() {
        wx.showModal({
          title: '提示',
          content: '请先登录，再进行操作！',
          showCancel: true,
          cancelColor: '',
          cancelText: '暂不登录',
          confirmText: '立即登录',
          success(res) {
            if (res.confirm) {
              // console.log('用户点击立即登录')
              wx.navigateTo({
                url: '/pages/login/login',
              })
            } else if (res.cancel) {
              // console.log('用户点击暂不登录')
              wx.switchTab({
                url: '/pages/index/index',
              })
            }
          },
          fail(e) {
            console.log(e);
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    // 页面卸载时设置插件选点数据为null，防止再次进入页面，geLocation返回的是上次选点结果
    chooseLocation.setLocation(null);
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})