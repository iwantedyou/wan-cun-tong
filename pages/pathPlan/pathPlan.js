// pages/pathPlan/pathPlan.js
const app = getApp()
import { request } from '../../utils/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight+45, 
    icon: 'arrow-down',
    hiddenMore: true,
    activeNames: [],
    // 县或去
    coutry: '',
    // 起点经度
    fromLon: '',
    // 起点维度
    fromLat: '',
    // 起点名字
    fromName: '',
    // 重点经度
    toLon: '',
    // 终点维度
    toLat: '',
    // 终点名字
    toName: '',
    // 线路类型  1直达  2转乘
    type: '',
    // 起点站路线名字
    lineName: '',
    // 直达线名字
    zhiLineName: '',
    // 转车线第一条线
    zhuanLineName1: '',
    // 转车线第二条线
    zhuanLineName2: ''
  },
  goBack() {
    wx.navigateBack({
      delta: 1
    })
  },
  showMore() {
    this.setData({
      icon: this.data.icon == 'arrow-down' ? 'arrow-up' : 'arrow-down',
      hiddenMore: this.data.hiddenMore ==  true ? false : true
    })
  },
  onChange(event) {
    this.setData({
      activeNames: event.detail,
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options);
    
    let params = app.globalData.pathDetailParams
    params.fromName = app.globalData.startData.title
    params.toName = app.globalData.endData.title
    console.log(params);
    
    if(params) {
      request({
        url: '/passengerTraffic-applet/applet/indexApi/testSearchLineDesc',
        method: 'post',
        data: params
      }).then(res => {
        console.log(res);
        this.setData({
          fromName: res.data.fromName,
          toName: res.data.toName
        })
        if(res.data.type == 1) {
          this.setData({
            zhiLineName: app.globalData.pathDetailParams.lineName,
          })
        }
        if(res.data.type == 2) {
          this.setData({
            zhuanLineName1: app.globalData.pathDetailParams.oneLineName,
            zhuanLineName2: app.globalData.pathDetailParams.twoLineName
          })
          
          
        }
        this.setData({
          type: res.data.type,
          pathDetail: res.data
        })
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})