import { request } from "../../../utils/api";

// pages/home/search/search.js
const app = getApp()
const chooseLocation = requirePlugin('chooseLocation');
const util = require('../../../utils/WSCoordinate')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    statusBarHeight1: app.globalData.statusBarHeight+45, 
    // 选择的起点位置
    startAddr: '请选择起点',
    // 起点站点id
    // startId: '',
    startLat: '',
    startLng: '',
    // 选择的重点位置
    endAddr: '请选择终点',
    // 终点站点id
    // endId: '',
    endLat: '',
    endLng: '',
    // 判断选择的是起点还是终点  1 起点  2 终点
    tag: 1,
    // 线路集合
    pathList: [],
    showPaths: 'hidden',
    showZhiPaths: 'hidden',
    showZhuanPaths: 'hidden',
    // 直达线路列表
    zhiList: [],
    // 转车线路列表
    zhuanList: [],
    // 跳转路线详情所需参数
    pathDetailParams: {}
  },
  goBack() {
    wx.navigateBack({
      delta: 1
    })
  },
  start() {
    // this.setData({
    //   tag: 1
    // })
    // const key = 'DUFBZ-OYF6D-RYR4L-HYKJD-JUR7Z-SLFQ5'; //使用在腾讯位置服务申请的key
    // const referer = 'bus'; //调用插件的app的名称
    // const location = JSON.stringify({
    //   latitude: this.data.latitude,
    //   longitude: this.data.longitude
    // });
    // // const category = '生活服务,娱乐休闲';
    // wx.navigateTo({
    //   url: `plugin://chooseLocation/index?key=${key}&referer=${referer}&location=${location}`
    // });
    app.globalData.tag = 1
    wx.redirectTo({
      url: '/pages/searchMap/searchMap?tag=1',
    })
  },
  end() {
    // this.setData({
    //   tag: 2
    // })
    // const key = 'MSPBZ-V5ARU-7VXVP-4MSXQ-KD2MZ-WWBMP'; //使用在腾讯位置服务申请的key
    // const referer = 'bus'; //调用插件的app的名称
    // const location = JSON.stringify({
    //   latitude: this.data.latitude,
    //   longitude: this.data.longitude
    // });
    // // const category = '生活服务,娱乐休闲';
    // wx.navigateTo({
    //   url: `plugin://chooseLocation/index?key=${key}&referer=${referer}&location=${location}`
    // });
    app.globalData.tag = 2
    wx.redirectTo({
      url: '/pages/searchMap/searchMap?tag=2',
    })
  },
  change() {
    if(this.data.startAddr == '请选择起点' || this.data.endAddr == '请选择终点') {
      wx.showToast({
        title: '请选择起点和终点',
        icon: 'none'
      })
      return
    }
    this.setData({
      // startAddr: this.data.endAddr,
      // startId: this.data.endId,
      // endAddr: this.data.startAddr,
      // endId: this.data.startId
      startAddr: this.data.endAddr,
      startLat: this.data.endLat,
      startLng: this.data.endLng,
      endAddr: this.data.startAddr,
      endLat: this.data.startLat,
      endLng: this.data.startLng,
    })
  },
  // 点击搜索按钮
  search() {
    // console.log(app.globalData);
    if(this.data.startAddr == '请选择起点' || this.data.endAddr == '请选择终点') {
      wx.showToast({
        title: '请选择起点和终点',
        icon: 'none'
      })
      return
    }
    // request({
    //   url: '/passengerTraffic-applet/applet/indexApi/searchLine',
    //   method: 'POST',
    //   data: {
    //     startId: this.data.startId,
    //     endId: this.data.endId
    //   }
    // }).then(res => {
    //   console.log(res);
    //   // 吧路线Id传过去
    //   // this.setData({
    //   //   pathList: res.data,
    //   //   showPaths: 'visible'
    //   // })
    //   // wx.navigateTo({
    //   //   url: '/pages/home/pathDetail/pathDetail',
    //   // })
    // })

    wx.showLoading({
      title: '搜索中...',
      mask: true
    })
    let startrobj = util.transformFromGCJToWGS(this.data.startLat - 0, this.data.startLng - 0)
    let endobj = util.transformFromGCJToWGS(this.data.endLat - 0, this.data.endLng - 0)
    request({
      url: '/passengerTraffic-applet/applet/indexApi/testSearchLine',
      method: 'POST',
      data: {
        // fromLon: this.data.startLng,
        fromLon: startrobj.longitude,
        // fromLat: this.data.startLat,
        fromLat: startrobj.latitude,
        fromName: this.data.startAddr,
        // toLon: this.data.endLng,
        toLon: endobj.longitude,
        // toLat: this.data.endLat,
        toLat: endobj.latitude,
        toName: this.data.endAddr,
        stationCountry: app.globalData.selectAddr.country
      }
    }).then(res => {
      console.log(res);
      wx.hideLoading()
      if(res.code != 1) {
        wx.showToast({
          title: res.msg,
          icon: 'none',
          duration: 2500
        })
        return
      }
      this.setData({
        showPaths: 'visible',
        lineList: res.data.lineList
      })
      // this.setData({
      //   pathDetailParams: {
      //     fromStationName: res.data.fromStationName,
      //     toStationName: res.data.toStationName,
      //     fromStationId: res.data.fromStationId,
      //     toStationId: res.data.toStationId,
          
      //     fromWalkingDistance: res.data.fromWalkingDistance,
      //     toWalkingDistance: res.data.toWalkingDistance,
          
      //   }
      // })
      // if(res.data.zhiList.length != 0) {
      //   this.setData({
      //     zhiList: res.data.zhiList,
      //     showZhiPaths: 'visible'
      //   })
      // }
      // if(res.data.zhuanList.length != 0) {
      //   this.setData({
      //     zhuanList: res.data.zhuanList,
      //     showZhuanPaths: 'visible'
      //   })
      // }
      
    })
    
  },
  // goPathDetail(e) {
  //   let lineId = e.currentTarget.dataset.lineid
  //   let type = e.currentTarget.dataset.type
  //   wx.navigateTo({
  //     url: '/pages/home/pathDetail/pathDetail?lineId=' + lineId + '&mkId=' + this.data.startId + '&type=' + type,
  //   })
  // },

  // 跳转到路线详情 直达
  goPathDetail1(e) {
    console.log(e);
    console.log(this.data.startAddr);
    console.log(this.data.endAddr);
    let obj = {
      type: 1,
      lineName: e.currentTarget.dataset.linename,
      zhiLineId: e.currentTarget.dataset.zhilineid,
      fromStationId: e.currentTarget.dataset.startstationid,
      toStationId: e.currentTarget.dataset.endstationid,  
      fromWalkingDistance: e.currentTarget.dataset.startwalk,
      toWalkingDistance: e.currentTarget.dataset.endwalk,
      fromStationName: e.currentTarget.dataset.startstationname, 
      toStationName: e.currentTarget.dataset.endstationname,   
      fromName: this.data.startAddr,
      toName: this.data.endAddr,
      oneType: e.currentTarget.dataset.onetype,  
      centerStationId:  e.currentTarget.dataset.middlestation
    }
    // let type = e.currentTarget.dataset.type
    // let obj = this.data.pathDetailParams
    
    // obj.type = type
    // if (type == 1) {
    //   obj.lineName = e.currentTarget.dataset.linename
    //   obj.zhiLineId = e.currentTarget.dataset.zhilineid
     
    // }
    // if (type == 2) {
    //   obj.lineName = e.currentTarget.dataset.linename
    //   obj.startLineId = e.currentTarget.dataset.startlineid
    //   obj.endLineId = e.currentTarget.dataset.endlineid
    // }
    // console.log(obj);
    app.globalData.pathDetailParams = obj
    wx.navigateTo({
      url: '/pages/pathPlan/pathPlan',
    })
  },
  // 跳转到路线详情 转乘
  goPathDetail2(e) {
    console.log(e);
    let obj = {
      type: 2,
      startLineId: e.currentTarget.dataset.onelineid,
      endLineId:  e.currentTarget.dataset.twolineid,  
      oneLineName: e.currentTarget.dataset.onelinename,
      twoLineName: e.currentTarget.dataset.twolinename,
      fromStationId: e.currentTarget.dataset.startstationid,
      toStationId: e.currentTarget.dataset.endstationid,  
      fromWalkingDistance: e.currentTarget.dataset.startwalk,
      toWalkingDistance: e.currentTarget.dataset.endwalk,
      fromStationName: e.currentTarget.dataset.startstationname, 
      toStationName: e.currentTarget.dataset.endstationname,   
      fromName: this.data.startAddr,
      toName: this.data.endAddr,
      oneType: e.currentTarget.dataset.onetype,   
      twoType: e.currentTarget.dataset.twotype,   
      centerStationId:  e.currentTarget.dataset.middlestation
    }
    // let type = e.currentTarget.dataset.type
    // let obj = this.data.pathDetailParams
    
    // obj.type = type
    // if (type == 1) {
    //   obj.lineName = e.currentTarget.dataset.linename
    //   obj.zhiLineId = e.currentTarget.dataset.zhilineid
     
    // }
    // if (type == 2) {
    //   obj.lineName = e.currentTarget.dataset.linename
    //   obj.startLineId = e.currentTarget.dataset.startlineid
    //   obj.endLineId = e.currentTarget.dataset.endlineid
    // }
    // console.log(obj);
    app.globalData.pathDetailParams = obj
    wx.navigateTo({
      url: '/pages/pathPlan/pathPlan',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options);
    // let that = this
    // if(options.country) {
    //   this.setData({
    //     country: options.country
    //   })
    // }
    if(app.globalData.startData) {
      this.setData({
        startAddr: app.globalData.startData.title,
        // startId:app.globalData.startData.stationId
        startLat:app.globalData.startData.latitude,
        startLng:app.globalData.startData.longitude
      })
    } 
    if(app.globalData.endData) {
      this.setData({
        endAddr: app.globalData.endData.title,
        // endId:app.globalData.endData.stationId
        endLat:app.globalData.endData.latitude,
        endLng:app.globalData.endData.longitude
      })
    } 
    
    // wx.getLocation({
    //   success(res) {
    //     that.setData({
    //       latitude: res.latitude,
    //       longitude: res.longitude
    //     })
    //   }
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // const location = chooseLocation.getLocation(); // 如果点击确认选点按钮，则返回选点结果对象，否则返回null
    // console.log(location);
    // if(location && this.data.tag == 1) {
    //   this.setData({
    //     startAddr: location.name
    //   })
    // }
    // if(location && this.data.tag == 2) {
    //   this.setData({
    //     endAddr: location.name
    //   })
    // }

  }

})